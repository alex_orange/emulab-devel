#!/usr/bin/perl -wT
#
# Copyright (c) 2010-2021 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
package libptop;

use strict;
use English;
use Carp;
use Exporter;
use vars qw(@ISA @EXPORT @EXPORT_OK);

@ISA = "Exporter";
@EXPORT = qw( );

sub Create($)
{
    my ($class) = @_;
    my $self = {};

    $self->{'NODES'} = {};
    $self->{'LINKS'} = {};

    bless($self, $class);

    return $self;
}

# Accessors
sub nodes($) { return $_[0]->{'NODES'}; }
sub links($) { return $_[0]->{'LINKS'}; }

# Add new nodes and links
sub CreateNode($$$)
{
    my ($self, $node_id, $row) = @_;
    my $node = libptop::pnode->Create($node_id, $row);
    $self->nodes()->{$node_id} = $node;
    return $node;
}

###############################################################################
# Physical Nodes. These contain the all of the per-node state used to
# generate ptop or xml files.

package libptop::pnode;
use Carp;
use vars qw($AUTOLOAD);

sub Create($$$)
{
    my ($class, $node_id, $row) = @_;

    my $self = {};

    $self->{'NODE_ID'}    = $node_id;
    $self->{'ROW'}        = $row;
    $self->{'HASH'}       = {};

    bless($self, $class);
    return $self;
}

AUTOLOAD {
    my $self  = $_[0];
    my $type  = ref($self) or confess "$self is not an object";
    my $name  = $AUTOLOAD;
    $name =~ s/.*://;   # strip fully-qualified portion

    # A DB row proxy method call.
    if (exists($self->{'ROW'}->{$name})) {
	return $self->{'ROW'}->{$name};
    }
    # Or it is for a local storage slot.
    if ($name =~ /^_.*$/) {
	if (scalar(@_) == 2) {
	    return $self->{'HASH'}->{$name} = $_[1];
	}
	elsif (exists($self->{'HASH'}->{$name})) {
	    return $self->{'HASH'}->{$name};
	}
	# No noise for a local storage slot
	return undef;
    }
    carp("No such slot '$name' field in class $type");
    return undef;
}
# Accessors
sub name($)       { return $_[0]->{'NODE_ID'}; }
sub subnode_of($) { return $_[0]->{'ROW'}->{'phys_nodeid'}; }
sub is_subnode($) { return $_[0]->{'ROW'}->{'issubnode'}; }
sub is_remote($)  { return $_[0]->{'ROW'}->{'isremotenode'}; }

sub DESTROY {
    my $self = shift;

    $self->{'ROW'}     = undef;
    $self->{'HASH'}    = undef;
}

1;
