<?php
#
# Copyright (c) 2000-2022 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
chdir("..");
include_once("webtask.php");
chdir("apt");

function Do_Submit()
{
    global $this_user;
    global $ajax_args;
    global $SUPPORT, $APTMAIL;

    if (!isset($ajax_args["formfields"])) {
	SPITAJAX_ERROR(-1, "Missing formfields");
	return -1;
    }
    $formfields = $ajax_args["formfields"];
    $errors     = array();

    #
    # All fields required,
    #
    $required = array("description", "frequencies", "encodings", "pid");

    foreach ($required as $field) {
	if (!isset($formfields[$field]) || trim($formfields[$field]) == "") {
	    $errors[$field] = "Missing field";
	}
    }
    if (count($errors)) {
	SPITAJAX_ERROR(2, $errors);
	return;
    }
    $pid = $formfields["pid"];
    if (!TBvalid_newpid($pid)) {
	SPITAJAX_ERROR(-1, "Not a valid project name");
	return;
    }
    $project = Project::Lookup($pid);
    if (!$project) {
	SPITAJAX_ERROR(-1, "Not a valid project");
	return;
    }
    if (! TBvalid_fulltext($formfields["description"])) {
	$errors["description"] = TBFieldErrorString();
    }
    if (! TBvalid_description($formfields["frequencies"])) {
	$errors["frequencies"] = TBFieldErrorString();
    }
    if (! TBvalid_description($formfields["encodings"])) {
	$errors["encodings"] = TBFieldErrorString();
    }
    if (count($errors)) {
	SPITAJAX_ERROR(2, $errors);
	return;
    }
    # Allow for form precheck only. So JS code knows it will be fast.
    if (isset($ajax_args["checkonly"]) && $ajax_args["checkonly"]) {
        SPITAJAX_RESPONSE(0);
        return;
    }
    $email = $this_user->email();

    TBMAIL($APTMAIL,
           "OTA Agreement form for project $pid submitted",
           "OTA Agreement form for project $pid submitted\n\n".
           "Description:\n" . $formfields["description"] . "\n\n" .
           "Frequencies:\n" . $formfields["frequencies"] . "\n\n" .
           "Encodings:\n" . $formfields["encodings"] . "\n\n\n".
           $project->PortalURL() . "\n",
           "From: $email\n".
           "Bcc: $email");

    $this_user->SetOtaAgreed();
    SPITAJAX_RESPONSE(0);
}

#
# More project members, not leaders.
#
function Do_Agree()
{
    global $this_user;

    #
    # Current user agrees.
    #
    $this_user->SetOtaAgreed();
    SPITAJAX_RESPONSE(0);
}

# Local Variables:
# mode:php
# End:
?>
