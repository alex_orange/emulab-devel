<?php
#
# Copyright (c) 2000-2022 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
chdir("..");
include_once("webtask.php");
chdir("apt");
include_once("profile_defs.php");
include_once("gitrepo.ajax");
include_once("checkscript.ajax");

#
# Return info about a profile.
#
function Do_GetProfile()
{
    global $this_user;
    global $ajax_args;
    $isguest = 0;

    #
    # We allow guests on this one
    #
    if (! isset($this_user)) {
        $isguest = 1;
    }
    
    if (!isset($ajax_args["profile"])) {
	SPITAJAX_ERROR(1, "Missing profile");
	return;
    }
    $target = $ajax_args["profile"];

    if (! (IsValidUUID($target) || IsValidHash($target))) {
	SPITAJAX_ERROR(1, "Not a valid profile target");
	return;
    }
    $profile = Profile::Lookup($target);
    if (!$profile) {
	SPITAJAX_ERROR(1, "No such profile $target");
	return;
    }
    if ($isguest) {
        if (!$profile->ispublic()) {
            SPITAJAX_ERROR(1, "This profile is not publicly accessible!");
            return;
        }
    }
    elseif (! ($profile->CanView($this_user) || IsValidHash($target) ||
               ISFOREIGN_ADMIN())) {
	SPITAJAX_ERROR(1, "Not enough permission");
	return;
    }
    $blob = array();

    $blob["profile_name"]        = $profile->name();
    $blob["profile_version"]     = $profile->version();
    if ($isguest) {
        $blob["profile_creator"] = $profile->anonCreator();
    }
    else {
        $blob["profile_creator"] = $profile->creator();
        $blob["profile_updater"] = $profile->updater();
        $blob["hashkey"]         = $profile->hashkey();
    }
    $blob["profile_public"]      = $profile->ispublic() ? 1 : 0;
    $blob["profile_pid"]         = $profile->pid();
    $blob["profile_created"]     = DateStringGMT($profile->created());
    $blob["profile_published"]   = DateStringGMT($profile->published());
    $blob["profile_version_url"] = $profile->URL();
    $blob["profile_profile_url"] = $profile->ProfileURL();
    if ($profile->rspec() && $profile->rspec() != "") {
        $blob["profile_rspec"] = $profile->rspec();
    }
    if ($profile->script() && $profile->script() != "") {
        $blob["profile_script"] = $profile->script();
    }
    if ($profile->repourl() && $profile->repourl() != "") {
        $blob["profile_repourl"] = $profile->repourl();
        $blob["profile_refhash"] = $profile->repohash();
        $blob["profile_refspec"] = $profile->reporef();
    }
    else {
        $blob["profile_repourl"] = null;
    }
    if ($profile->isParameterized()) {
        $blob["paramdefs"] = json_decode($profile->paramdefs());
    }
    $latest_profile = Profile::Lookup($profile->profile_uuid());
    $blob["latest_uuid"] = $latest_profile->uuid();
    $blob["latest_version"] = $latest_profile->version();
    if ($this_user && $profile->CanEdit($this_user)) {
        $blob["profile_hashkey"] = $latest_profile->hashkey();
    }
    SPITAJAX_RESPONSE($blob);
}

#
# Return paramsets for a profile
#
function Do_GetParamsets()
{
    global $this_user;
    global $ajax_args;

    if (! isset($this_user)) {
	SPITAJAX_ERROR(1, "Not enough permission");
	return;
    }
    if (!isset($ajax_args["uuid"])) {
	SPITAJAX_ERROR(1, "Missing profile uuid");
	return;
    }
    $profile = Profile::Lookup($ajax_args["uuid"]);
    if (!$profile) {
	SPITAJAX_ERROR(1, "Unknown profile uuid");
	return;
    }
    if (!$profile->CanView($this_user) && !(ISADMIN() || ISFOREIGN_ADMIN())) {
	SPITAJAX_ERROR(1, "Not enough permission");
	return;
    }
    $paramsets = $profile->Paramsets($this_user);
    if ($paramsets && count($paramsets["owner"])) {
        SPITAJAX_RESPONSE($paramsets["owner"]);
    }
    else {
        SPITAJAX_RESPONSE(null);
    }
}

#
# Get source from repository, run the script.
#
function Do_GetSource()
{
    global $this_user;
    global $ajax_args;
    $refspec = "";
    
    if (!isset($ajax_args["refspec"])) {
	SPITAJAX_ERROR(1, "Missing branch or tag name");
	return;
    }
    if ($ajax_args["refspec"] != null) {
        if (!TBvalid_refspec($ajax_args["refspec"])) {
            SPITAJAX_ERROR(1, "Invalid refspec");
            return;
        }
        $refspec = $ajax_args["refspec"];
    }
    if (!isset($ajax_args["uuid"])) {
	SPITAJAX_ERROR(1, "Missing profile");
	return;
    }
    $profile = Profile::Lookup($ajax_args["uuid"]);
    if (!$profile) {
	SPITAJAX_ERROR(1, "Not a valid profile");
	return;
    }
    if (!isset($this_user)) {
        if (!$profile->ispublic()) {
            SPITAJAX_ERROR(1, "This profile is not publicly accessible!");
            return;
        }
    }
    elseif (! ($profile->CanView($this_user) || ISFOREIGN_ADMIN())) {
	SPITAJAX_ERROR(1, "Not enough permission");
	return;
    }
    $blob = null;
    if (GetRepoSource($profile, $refspec, $blob)) {
        return;
    }
    # Badly named, need to fix GetRepoSource.
    $script = $blob["script"];

    if (! (preg_match("/^(import|from)/m", $script) ||
           preg_match("/^source tb_compat/m", $script))) {
        $blob["rspec"] = $script;
        unset($blob["script"]);
        SPITAJAX_RESPONSE($blob);
        return;
    }

    #
    # And run the script.
    #
    $warningsfatal = false;
    $usenewgenilib = false;
    $getparams     = true;
    
    if (CheckScript($profile, $script, null, 
                    $warningsfatal, $usenewgenilib,
                    $refspec, $getparams, null, $blob) != 0) {
        # Error already spit.
        return;
    }
    SPITAJAX_RESPONSE($blob);
}

# Local Variables:
# mode:php
# End:
?>

